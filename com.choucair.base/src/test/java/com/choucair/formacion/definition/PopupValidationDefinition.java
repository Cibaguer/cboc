package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.PopupValidationSteps;
import com.choucair.formacion.steps.colorlibFormValidationSteps;


import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import net.thucydides.core.annotations.Steps;

public class PopupValidationDefinition {
	
	@Steps
	PopupValidationSteps popupvalidationsteps;
	
	@Steps
	
	colorlibFormValidationSteps colorlibFormValidationSteps;
	
	
	@Given("^Autentico colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autentico_colorlib_con_usuario_y_clave(String usuario, String clave)
	{
	  popupvalidationsteps.login_Colorlib(usuario, clave);
	    
}
	
	@Given("^Ingreso a la funcionalidad forms validation$")
	public void ingreso_a_la_funcionalidad_forms_validation() throws Throwable {
	    
		popupvalidationsteps.ingresar_form_validation();
		
		
	  //  throw new PendingException();
	}
	
	
/*	@Given("^ingreso a la funcionalidad forms validation$")
	public void ingreso_a_la_funcionalidad_forms_validation() throws Throwable {
		
		popupvalidationsteps.ingresar_form_validation();
		
		//PopupValidationSteps.ingresar_form_validation();
	    
	}
	*/
	@When("^Diligencio formulario Popup validation$")
	public void diligencio_formulario_Popup_validation(DataTable dtDatosforms) {
	    
		List<List<String>> data=dtDatosforms.raw();
        for(int i=1;i<data.size();i++) {
        	colorlibFormValidationSteps.diligenciar_popup_datos_tabla(data, i);
           try {
        	   Thread.sleep(5000);
           }catch(InterruptedException e) {}
           
        }
		
		
	    //throw new PendingException();
	}
	
/*	@When("^diligencio formulario popup validation$")
	public void diligencio_formulario_popup_validation(DataTable dtforms) throws Throwable {
	       
        List<List<String>> data=dtforms.raw();
        for(int i=1;i<data.size();i++) {
        	colorlibFormValidationSteps.diligenciar_popup_datos_tabla(data, i);
           
           
        }
	
	}
	*/
	@Then("^verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso()  {
		
		colorlibFormValidationSteps.verificar_ingresar_datos_formulario_exitoso();
		
	    
	}
	
	@Then("^verificar que se presente Globo Informativo de Validacion\\.$")
	public void verificar_que_se_presente_Globo_Informativo_de_Validacion()  {
		colorlibFormValidationSteps.verificar_ingresar_datos_formulario_con_exitoso();
	}

	
}
