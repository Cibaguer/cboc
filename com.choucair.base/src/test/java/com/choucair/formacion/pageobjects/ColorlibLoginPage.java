package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


@DefaultUrl ("https://Colorlib.com/polygon/metis/login.html")

public class ColorlibLoginPage extends PageObject {
	
	//campo usuario
	
	@FindBy(xpath="//*[@id='login']/form/input[1]")
	public WebElementFacade txtUsername;

	// campo password
	@FindBy(xpath="//*[@id='login']/form/input[2]")
	public WebElementFacade txtpassword;
	
	//Boton
	@FindBy(xpath="//*[@id='login']/form/button")
	public WebElementFacade btnSignIn;
	
	//label del home a verificar
	@FindBy(xpath="//*[@id='bootstrap-admin-template']")
	public WebElementFacade lblhomePpal;
	
	public void IngresarDatos(String strusuario, String strpass) {
		txtUsername.sendKeys(strusuario);
		txtpassword.sendKeys(strpass);
		btnSignIn.click();
		
}

	public void VerificaHome() {
		String labelv="Bootstrap-Admin-Template";
		String strmensaje=lblhomePpal.getText();
		assertThat(strmensaje,containsString(labelv));
	}
	

	
}
