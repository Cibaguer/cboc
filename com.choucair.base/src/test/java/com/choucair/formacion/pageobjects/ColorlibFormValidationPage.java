package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*; 

public class ColorlibFormValidationPage extends PageObject {

	//Campo required
	
	@FindBy(xpath="//INPUT[@id='req']")
	public WebElementFacade txtrequired;
	
	//campo seleccion deporte 1
	@FindBy(xpath="//SELECT[@id='sport']")
	public WebElementFacade cmbSport1;
	
	//campo seleccion deporte 2
		@FindBy(xpath="//*[@id=\'sport2\']")
		public WebElementFacade cmbSport2;
	
	//campo URL
		@FindBy(xpath= "//INPUT[@id='url1']")
		public WebElementFacade txtUrl;
		
	//campo Email
		@FindBy(xpath= "//INPUT[@id='email1']")
		public WebElementFacade txtEmail;
		
	//campo Password1
		@FindBy(xpath= "//INPUT[@id='pass1']")
		public WebElementFacade txtPass1;
				
	//campo Password2 	
		@FindBy(xpath= "//INPUT[@id='pass2']")
		public WebElementFacade txtPass2;
	//campo Minsize1
		@FindBy(xpath= "//INPUT[@id='minsize1']")
		public WebElementFacade txtMinsize1;
		
	//campo Minsize2
		@FindBy(xpath= "//INPUT[@id='maxsize1']")
		public WebElementFacade txtMinsize;	
		
	//campo Number
		@FindBy(xpath= "//INPUT[@id='number2']")
		public WebElementFacade txtNumber;	
	
	//campo Ip
		@FindBy(xpath= "//INPUT[@id='ip']")
		public WebElementFacade txtIp;	
		
	//campo Date
		
		@FindBy(xpath= "//INPUT[@id='date3']")
		public WebElementFacade txtDate;
		
	//campo Date txtDateEarlier
		
		@FindBy(xpath= "//INPUT[@id='past']")
		public WebElementFacade txtDateEarlier;
		
	//campo Date Validation 
		@FindBy(xpath= "//*[@id=\'popup-validation\']/div[14]/input")
		public WebElementFacade btnValidate;
		
	//globo informativo
		@FindBy(xpath= "(//DIV[@class='formErrorContent'])[1]")
		public WebElementFacade globoinformativo;

		
		public void Required(String datoPrueba) {
			
			txtrequired.click();
			txtrequired.clear();
			txtrequired.sendKeys(datoPrueba);
			
		}
		
		public void Select_Sport(String datoPrueba) {
			
			cmbSport1.click();
			cmbSport1.selectByVisibleText(datoPrueba);
			
			
			
		}
		
		public void Multiple_Select(String datoPrueba) {
            cmbSport2.selectByVisibleText(datoPrueba);
        }
		
		public void url(String datoPrueba) {
			txtUrl.click();
			txtUrl.clear();
			txtUrl.sendKeys(datoPrueba);
			
		}
		
		public void email (String datoPrueba) {
			txtEmail.click();
			txtEmail.clear();
			txtEmail.sendKeys(datoPrueba);
			
		}
		
		public void password (String datoPrueba)
		{
			txtPass1.click();
			txtPass1.clear();
			txtPass1.sendKeys(datoPrueba);
			
		}

				
	   public void confirm_password (String datoPrueba) {
		   
		   txtPass2.click();
		   txtPass2.clear();
		   txtPass2.sendKeys(datoPrueba);
		   
		   
	   }
	   
	   public void minimun_field_size(String datoPrueba) {
		   
		   txtMinsize1.click();
		   txtMinsize1.clear();
		   txtMinsize1.sendKeys(datoPrueba);
		   
		   
	   }
	   
	   public void maximun_field_size(String datoPrueba) {
		   
		   txtMinsize.click();
		   txtMinsize.clear();
		   txtMinsize.sendKeys(datoPrueba);
		   
	   }
		
	   public void Number (String datoPrueba) {
		   
		   txtNumber.click();
		   txtNumber.clear();
		   txtNumber.sendKeys(datoPrueba);
		   
	   }
	   
	   public void Ip(String datoPrueba) {
		   
		   txtIp.click();
		   txtIp.clear();
		   txtIp.sendKeys(datoPrueba);
		   
	   }
	   
     public void Date(String datoPrueba) {
		  
    	   txtDate.click();
    	   txtDate.clear();
    	   txtDate.sendKeys(datoPrueba);
		   
	   }
     
     public void DateEarlier(String datoPrueba) {
		  
    	 txtDateEarlier.click();
    	 txtDateEarlier.clear();
    	 txtDateEarlier.sendKeys(datoPrueba);
		   
	   }
     
     public void validate() {
    	 
    	 btnValidate.click();
     }
     
     public void form_sin_errores() {
    	 
    	 assertThat(globoinformativo.isCurrentlyVisible(),is(false));
     }
     
     public void form_con_errores() {
    	 
    	 assertThat(globoinformativo.isCurrentlyVisible(),is(true)); 
     }
}
