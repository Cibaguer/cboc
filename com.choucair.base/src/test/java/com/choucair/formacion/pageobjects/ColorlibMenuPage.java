package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.serenitybdd.core.pages.PageObject;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import org.openqa.selenium.WebElement;

public class ColorlibMenuPage extends PageObject {
	

	//menu forms
	@FindBy(xpath="//*[@id=\'menu\' ]/li[6]/a")
	public WebElement menuForms;
	
	// Submenu Forms validation
	@FindBy(xpath="//*[@id=\'menu\']/li[6]/ul/li[2]/a")
	public WebElement menuFormValidation;

	//  Form validation-label informativo
		@FindBy(xpath="//*[@id=\'content\']/div/div/div[1]/div/div/header/h5")
		public WebElement lblFormvalidation;
		
		public void menuFormValidation() {
			
			menuForms.click();
			menuFormValidation.click();
			String strmensaje=lblFormvalidation.getText();
			assertThat(strmensaje,containsString("Popup Validation"));
			
			
		}
		
	
}
