package com.choucair.formacion.steps;

import net.thucydides.core.annotations.Step;
import com.choucair.formacion.pageobjects.ColorlibLoginPage;
import com.choucair.formacion.pageobjects.ColorlibMenuPage;

public class PopupValidationSteps {

	ColorlibLoginPage ColorlibLoginPage;
	
	ColorlibMenuPage ColorlibMenuPage ;
	
	
	@Step
	public void login_Colorlib(String strusuario,String strpass) {
	// a. abrir el navegador  con la URL de prueba
	ColorlibLoginPage.open();
	/* b. ingresar usuario demo
	 * c. ingresar password demo
	 * d. click en boton sign in *
	 */
	ColorlibLoginPage.IngresarDatos(strusuario, strpass );
	// verificar la autenticacion (label en home)
	ColorlibLoginPage.VerificaHome();
}
	
	@Step
	
	public void ingresar_form_validation() {
			
		ColorlibMenuPage.menuFormValidation();
			
	}
	
	
}
