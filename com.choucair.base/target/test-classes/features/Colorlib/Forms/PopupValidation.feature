#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Formulario popup validation
el usuario debe ingresar al formulario los datos requeridos.
cada campo del formulario realiza validacion de obligatoriedad,
longitu y formato,el sistema dee presentar las validaciones respectivas
para cada campo a traves un globo informativo.
 

  @casoexitoso
  Scenario: Diligencimiento exitoso del formulario popup validation,
           no presenta ningun mensaje validacion.
    Given Autentico colorlib con usuario "demo" y clave "demo"
    And Ingreso a la funcionalidad forms validation
    When Diligencio formulario Popup validation
    
    
    |Required|select|MultipleS1|MultipleS2|url                 |Email           |Password1|Pasword2|MinSize|MaxSize|Number|IP         |Date      |DateEarlier|
    |Valor 1 |Golf |Tennis    |    Golf  |http://www.valor1.com|valor1@gmail.com|valor1   |valor1  |123456 |123456 |-99.99|200.200.5.4|2018-01-22|2012/09/12 |
    
      
    Then verifico ingreso exitoso
    
    @casoAlterno
    
    Scenario: Diligenciamiento con errores del formulario Popup validation 
    se presenta en el globo informativo indicando error en el diligenciamiento en alguno de los campos.
    
    Given Autentico colorlib con usuario "demo" y clave "demo"
    And Ingreso a la funcionalidad forms validation
    When Diligencio formulario Popup validation
   
    |Required|select|MultipleS1|MultipleS2|url                 |Email           |Password1|Pasword2|MinSize|MaxSize|Number|IP         |Date      |DateEarlier|
    | |Golf |Tennis    |    Golf  |http://www.valor1.com|valor1@gmail.com|valor1   |valor1  |123456 |123456 |-99.99|200.200.5.4|2018-01-22|2012/09/12 |
    |valor1 |Choose a sport |Tennis    |    Golf  |http://www.valor1.com|valor1@gmail.com|valor1   |valor1  |123456 |123456 |-99.99|200.200.5.4|2018-01-22|2012/09/12 |

    Then verificar que se presente Globo Informativo de Validacion.
    
    